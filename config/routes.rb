Rails.application.routes.draw do
  
	resources :events, only: :create

	get 'issues/:number/events', to: 'issues#show'

end
