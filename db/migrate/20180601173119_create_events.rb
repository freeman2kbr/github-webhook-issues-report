class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.integer :number,	null: false
      t.json :json,			null: false

      t.timestamps
    end
  end
end
