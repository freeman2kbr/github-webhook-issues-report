# Octo Events

## 1. Preparação / Inicialização

Para iniciar, basta clonar o repositório e executar o bundler.

```console
git clone git@gitlab.com:freeman2kbr/github-webhook-issues-report.git

cd github-webhook-issues-report.git
bundle install
```

Modifique o arquivo *database.yml* inserindo as informações da sua instancia de Postgres. 
Em seguida, vamos criar o banco de dados e executar as migrações para criar a tabela de eventos.

```console
rake db:create
rake db:migrate
```

Feito isso, basta iniciar o servidor:

```console
rails s -p 3000 -b '0.0.0.0'
```

## 2. End points

Essa aplicação responde a dois endpoints:

*`/events` - utilizada exclusivamente pelo GitHub para inserir os eventos do repositório;

*`/issues/:number/events` - utilizado para consultar um ou mais eventos de uma ~issue~ identificada pelo número.

Para que a rota de eventos funcione, é necessário acessar o seu repositório no GitHub, no menu Settings > Webhooks inserindo o endereço do seu servidor local (ou do NGrok) no campo de ~Payload URL~.

É necessario também inserir uma chave segredo para garantir que o serviço não seja utilizado por terceiros que não o GitHub. Para isso, preencha o campo ~Secret~ e informe o mesmo valor no arquivo *config.yml*.

## 3. Gerando eventos

Para que os eventos sejam gerados, basta interagir com as ~issues~ no repositório, qualquer ação dentro delas irá gerar um evento que será associado ao número da ~issue~ e devidamente registrado em banco.

## 4. Consultando os eventos

Basta acessar o endpoint informando o número da issue para ter um relatório de todos os eventos gerados a partir dela (criação, edição, comentários, etc).

## 5. Testes Automatizados (RSpec)

Este repositório possui alguns testes que serviram de apoio durante o desenvolvimento, para executá-los basta digitar o seguinte comando no terminal:

```console
bundle exec rspec spec/controllers/*
```

## 6. Versões

* Ruby 2.5.1
* Rails 5.2.0
* RSpec 3.7.1