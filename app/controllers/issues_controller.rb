class IssuesController < ApplicationController

	def show
		events = Event.where(number: params[:number]).pluck(:json)

		if events.empty?
			render json: "Issue number #{ params[:number] } not found".to_json, status: :not_found
		else
			render json: events
		end
	end

end