class EventsController < ApplicationController

	def create
		if validate_signature request
			Event.create! validated_params(params)

			render json: {}, status: :created
		else
			render json: "Signatures didn't match!", status: :unauthorized
		end
	end

	private

	def validate_signature request
		signature = 'sha1=' + OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), APP_CONFIG['SECRET_TOKEN'], request.body.read)

  		return !request.headers['X-Hub-Signature'].nil? && Rack::Utils.secure_compare(signature, request.headers['X-Hub-Signature'])
	end

	def validated_params params
		# necessário reverter o parâmetro "action" ao valor original passado pelo GitHub pois o Rails modifica esse parâmetro para a lógica de roteamento
		if request.method == 'POST'
			params[:action] = request.request_parameters['action'];
		end

		# filtro para aceitar apenas eventos de issue
		{ number: params.require(:issue).require(:number), json: params }
	end

end