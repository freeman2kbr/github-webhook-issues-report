class ApplicationController < ActionController::Base

	skip_before_action :verify_authenticity_token

	respond_to :json

	rescue_from ActionController::ParameterMissing do |exception|
	  render json: {exception.param => "is required"}, status: 400
	end

end
