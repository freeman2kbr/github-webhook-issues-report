class Event < ApplicationRecord

	validates :number,			numericality: {greater_than: 0, less_than: 2147483648}
	validates :json,			presence: true

end